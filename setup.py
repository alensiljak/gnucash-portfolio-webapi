from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='gnucash-portfolio-webapi',
    version='0.0.2',
    description='Web API for GnuCash Portfolio',
    long_description=readme,
    author='Alen Siljak',
    author_email='alen.siljak@gmx.com',
    url='https://gitlab.com/alensiljak/gnucash-portfolio-webapi',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={
        'console_scripts': [
            'gpwebapi=gnucash_portfolio_webapi.app:run',
        ],
    }
)
