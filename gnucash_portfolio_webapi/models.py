"""
DAL model
"""
from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from flask_security import UserMixin, RoleMixin
import jwt
from datetime import datetime, timedelta

# Create database connection object
# db = SQLAlchemy(app)
db = SQLAlchemy()

# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    # __tablename__ == 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    # password_hash = db.Column(db.String(128))
    active = db.Column(db.Boolean())
    # confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def generate_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            # set up a payload with an expiration time
            payload = {
                'exp': datetime.utcnow() + timedelta(minutes=5),
                'iat': datetime.utcnow(),
                'sub': user_id
            }
            # create the byte string token using the payload and the SECRET key
            secret = current_app.config.get('SECRET_KEY')
            jwt_string = jwt.encode(
                payload,
                secret,
                algorithm='HS256'
            )
            return jwt_string

        except Exception as e:
            # return an error in string format if an exception occurs
            return str(e)

    @staticmethod
    def decode_token(token):
        """
        Validates the auth token.
        Decodes the access token from the Authorization header.
        :param auth_token:
        :return: integer|string
        """
        try:
            # try to decode the token using our SECRET variable
            secret = current_app.config.get('SECRET_KEY')
            # is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            payload = jwt.decode(token, secret)
            return payload['sub']
        except jwt.ExpiredSignatureError:
            # the token is expired, return an error string
            return "Expired token. Please login to get a new token"
        except jwt.InvalidTokenError:
            # the token is invalid, return an error string
            return "Invalid token. Please register or login"

    def password_is_valid(self, password):
        """
        Checks the password against it's hash to validates the user's password
        """
        from flask_bcrypt import Bcrypt

        return Bcrypt().check_password_hash(self.password, password)

    def save(self):
        """Save a user to the database.
        This includes creating a new user and editing one.
        """
        db.session.add(self)
        db.session.commit()
