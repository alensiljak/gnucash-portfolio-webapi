"""
Accounts
"""
from flask import Blueprint, jsonify
import logging
from gnucash_portfolio_webapi.auth import auth


# This instance of a Blueprint that represents the authentication blueprint
account_blueprint = Blueprint('account', __name__, url_prefix='/account')


@account_blueprint.route('/favourites')
#@auth.login_required
def fav_accounts():
    """ Currently returning dummy data """
    from gnucash_portfolio import BookAggregate

    result = []
    with BookAggregate() as book:
        favs = book.accounts.get_favourite_accounts()
        # logging.debug(favs)
        for fav in favs:
            # str(fav.get_balance())
            # balance = f"{fav.get_balance():,.2f}"
            balance = float(fav.get_balance())
            result.append({ "name": fav.fullname, "balance": balance })

    response = jsonify(result)
    response.status_code = 200
    return response
