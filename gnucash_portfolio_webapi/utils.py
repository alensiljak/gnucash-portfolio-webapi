'''
Various utilities that do not have a home yet.
'''
from json import JSONEncoder


class MyEncoder(JSONEncoder):
    '''
    Custom JSON encoder, which serializes objects 
    Ref: https://stackoverflow.com/questions/3768895/how-to-make-a-class-json-serializable
    '''

    def default(self, o):
        ''' the default converter method '''
        from datetime import datetime
        from decimal import Decimal

        if isinstance(o, datetime):
            return o.__str__()
        elif isinstance(o, Decimal):
            return o.to_eng_string()
        else:
            return o.__dict__  