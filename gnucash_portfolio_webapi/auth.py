"""
Authentication
"""
from flask import Blueprint, request, make_response, jsonify
from flask_httpauth import HTTPTokenAuth
from gnucash_portfolio_webapi.models import User

auth = HTTPTokenAuth('Bearer')

@auth.verify_token
def verify_token(token):
    """ Validate token """
    result = User.decode_token(token)
    if result == 1:
        return True
    else:
        return False



# This instance of a Blueprint that represents the authentication blueprint
auth_blueprint = Blueprint('auth', __name__, url_prefix='/auth')

@auth_blueprint.route('/register', methods=['POST'])
def register():
    # Query to see if the user already exists
    #email_param = request.data['email']
    email_param = request.form['email']
    if not email_param:
        raise ValueError("no email provided")

    user = User.query.filter_by(email=email_param).first()

    if not user:
        # There is no user so we'll try to register them
        try:
            #post_data = request.data
            post_data = request.form

            # Register the user
            email = post_data['email']
            password = post_data['password']
            user = User(email=email, password=password)
            user.save()

            response = {
                'message': 'You registered successfully. Please log in.'
            }
            # return a response notifying the user that they registered successfully
            return make_response(jsonify(response)), 201
        except Exception as e:
            # An error occured, therefore return a string message containing the error
            response = {
                'message': str(e)
            }
            #return make_response(jsonify(response)), 401
            return make_response(jsonify(response)), 500
    else:
        # There is an existing user. We don't want to register users twice
        # Return a message to the user telling them that they they already exist
        response = {
            'message': 'User already exists. Please login.'
        }

        return make_response(jsonify(response)), 202

@auth_blueprint.route('/login', methods=['POST'])
def login():
    id_field = "client_id" # 'email'
    pwd_field = "client_secret" # 'password'
    try:
        form_data = request.form
        # Get the user object using their email (unique to every user)
        user = User.query.filter_by(email=form_data[id_field]).first()

        # Try to authenticate the found user using their password
        if user and user.password_is_valid(form_data[pwd_field]):
            # Generate the access token. This will be used as the authorization header
            access_token = user.generate_token(user.id)
            if access_token:
                decoded = access_token.decode()
                response = {
                    'message': 'You logged in successfully.',
                    'access_token': access_token.decode()
                }
                return make_response(jsonify(response)), 200
        else:
            # User does not exist. Therefore, we return an error message
            response = {
                'message': 'Invalid email or password, Please try again'
            }
            return make_response(jsonify(response)), 401

    except Exception as e:
        # Create a response containing an string error message
        response = {
            'message': str(e)
        }
        # Return a server error using the HTTP Error Code 500 (Internal Server Error)
        return make_response(jsonify(response)), 500
