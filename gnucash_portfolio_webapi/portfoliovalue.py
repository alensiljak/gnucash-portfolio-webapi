'''
Portfolio Value Report
'''
try:
    import simplejson as json
except ImportError:
    import json
from flask import Blueprint
from flask_cors import cross_origin

pv_blueprint = Blueprint('portfoliovalue', __name__, url_prefix='/portfoliovalue')


@pv_blueprint.route('/report')
@cross_origin()
def report():
    ''' returns the report '''
    from gnucash_portfolio_webapi.utils import MyEncoder

    # todo cache server-side?

    view_model = __generate_report()
    output = MyEncoder().encode(view_model)

    return output

def __generate_report():
    from gnucash_portfolio.reports import portfolio_value

    input = portfolio_value.PortfolioValueInputModel()
    view_model: portfolio_value.PortfolioValueViewModel = portfolio_value.run(input)
    return view_model
