""" The flask application's entry point """
import os, string
from flask import Flask, request, jsonify, abort
#from flask_api import FlaskAPI
from flask_cors import CORS
from flask_security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin
from gnucash_portfolio_webapi.models import db, User, Role, roles_users
from gnucash_portfolio_webapi.auth import auth


def get_pass(password_len=12):
    """ Generate a random password """
    new_password = None
    symbols = '^&%$@#!'
    chars = string.ascii_lowercase +\
        string.ascii_uppercase +\
        string.digits +\
        symbols

    while new_password is None or \
            new_password[0] in string.digits or \
            new_password[0] in symbols:
        new_password = ''.join([chars[ord(os.urandom(1)) % len(chars)]
                                for i in range(password_len)])
    return new_password


def apply_blueprints(app):
    from gnucash_portfolio_webapi.auth import auth_blueprint
    app.register_blueprint(auth_blueprint)
    from gnucash_portfolio_webapi.account import account_blueprint
    app.register_blueprint(account_blueprint)
    from gnucash_portfolio_webapi.assetallocation import aa_blueprint
    app.register_blueprint(aa_blueprint)

    from gnucash_portfolio_webapi.portfoliovalue import pv_blueprint
    app.register_blueprint(pv_blueprint)


def create_app(config_name):

    # Create app
    app = Flask(__name__)
    app.config['DEBUG'] = True
    # generate a random key every time.
    secret_key = get_pass(32)
    #app.config['SECRET_KEY'] = 'super-super-secret'
    app.config['SECRET_KEY'] = secret_key
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECURITY_PASSWORD_SALT'] = 'a little bit of salt'

    # Now initialize the data access layer.
    db.init_app(app)

    # Blueprints
    apply_blueprints(app)
    
    return app


app = create_app(None)

CORS(app)

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
# security = Security(app, user_datastore)

# Create a user to test with


@app.before_first_request
def create_user():
    db.create_all()
    # todo: remove this after tests
    # user_datastore.create_user(
    #     email='alen', password='$2y$12$ewH0KiznsgJDwiA1bpr1cuSf1NrY9wMCTGviHzmvPCgxmh762RkFa')
    # db.session.commit()


@app.route("/")
def index():
    return "Hello World!"


@app.route('/protected')
@auth.login_required
def protected():
    return "welcome to the machine"


def run():
    """ Run the web server """
    # Use SSL
    # ssl_context='adhoc'
    app.run(host='0.0.0.0')
    # port=4000


if __name__ == "__main__":
    run()
