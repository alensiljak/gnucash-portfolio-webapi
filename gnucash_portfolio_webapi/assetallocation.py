"""
Asset Allocation
"""
try:
    import simplejson as json
except ImportError:
    import json
from flask import Blueprint
from flask_cors import cross_origin
from gnucash_portfolio_webapi.auth import auth

aa_blueprint = Blueprint('asset_allocation', __name__, url_prefix='/assetallocation')

@aa_blueprint.route('/generate')
#@auth.login_required
@cross_origin()
def generate():
    """ Create Asset Allocation report """
    from asset_allocation import AppAggregate, HtmlFormatter, AsciiFormatter

    app = AppAggregate()
    model = app.get_asset_allocation()

    #fmt = HtmlFormatter()
    fmt = AsciiFormatter()
    output = fmt.format(model)
    #return json.dumps(model)
    return output
    