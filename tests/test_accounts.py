"""
Tests for the accounts
ref: https://scotch.io/tutorials/build-a-restful-api-with-flask-the-tdd-way
"""
import pytest
import logging
try:
    import simplejson as json
except ImportError:
    import json
from gnucash_portfolio_webapi.app import app, db

def setup_module(module):
    logging.debug('module init')


class TestAccounts(object):
    """ Test accounts """

    def test_favourite_accounts(self, app, auth_header):
        """ list the favourite accounts in JSON """
        res = app.test_client().get('/account/favourites', headers=auth_header)

        assert res is not None, "successful connection"
        assert res.status_code != 404, "the destination must exist"
        assert res.is_json == True, "the result must be JSON"
        assert res.json is not None

    @classmethod
    def teardown_class(cls):
        pass
