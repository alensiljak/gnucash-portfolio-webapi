"""
Tests for authentication
"""
import pytest
try:
    import simplejson as json
except ImportError:
    import json
# from gnucash_portfolio_webapi.app import create_app
from gnucash_portfolio_webapi.models import db, User, Role


class TestAuthentication(object):

    def test_password_check(self, app, user_data):
        """ Test password hash check """
        from flask_bcrypt import Bcrypt
        
        password = user_data['password']
        my_hash = user_data['hash']
        hash = Bcrypt().generate_password_hash(password, 12)
        result = Bcrypt().check_password_hash(my_hash, password)
        assert result == True

    def test_user_login(self, app, user_data):
        """Test registered user can login."""

        login_res = app.test_client().post('/auth/login', data=user_data)

        assert login_res.status_code != 404

        # get the results in json format
        result = json.loads(login_res.data.decode())
        
        # Test that the response contains success message
        assert result['message'] == "You logged in successfully."
        # Assert that the status code is equal to 200
        assert login_res.status_code == 200
        assert result['access_token']

    def test_non_registered_user_login(self, app):
        """Test non registered users cannot login."""
        # define a dictionary to represent an unregistered user
        not_a_user = {
            'email': 'not_a_user@example.com',
            'password': 'nope'
        }
        # send a POST request to /auth/login with the data above
        res = app.test_client().post('/auth/login', data=not_a_user)
        # get the result in json
        result = json.loads(res.data.decode())

        # assert that this response must contain an error message 
        # and an error status code 401(Unauthorized)
        assert res.status_code == 401
        assert result['message'] == "Invalid email or password, Please try again"

    def test_token(self, app, user_data):
        """ test the token """
        login_res = app.test_client().post('/auth/login', data=user_data)
        result = json.loads(login_res.data.decode())
        token = result['access_token']

        with app.app_context():
            decoded = User.decode_token(token)
            assert decoded == 1

    def test_favourite_accounts_invalid_token(self, app, user_data):
        """ list the favourite accounts in JSON """
        # login first
        login_res = app.test_client().post('/auth/login', data=user_data)
        access_token = json.loads(login_res.data.decode())['access_token']
        access_token += "e"

        res = app.test_client().get('/account/favourite', headers=dict(Authorization="Bearer " + access_token))

        assert res is not None, "successful connection"
        assert res.status_code == 401

    def test_favourite_accounts_valid_token(self, app, user_data):
        """ list the favourite accounts in JSON """
        # login first
        login_res = app.test_client().post('/auth/login', data=user_data)
        access_token = json.loads(login_res.data.decode())['access_token']

        res = app.test_client().get('/account/favourite', headers=dict(Authorization="Bearer " + access_token))

        assert res is not None, "successful connection"
        assert res.status_code == 200

    def test_favourite_accounts_header(self, app, auth_header):
        """ Get the full authentication header from test setup """

        res = app.test_client().get('/account/favourite', headers=auth_header)

        assert res is not None, "successful connection"
        assert res.status_code == 200
