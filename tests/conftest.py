"""
Test fixtures
"""
import pytest
try:
    import simplejson as json
except ImportError:
    import json
from flask import current_app
from flask_security import SQLAlchemyUserDatastore
from gnucash_portfolio_webapi.app import create_app
from gnucash_portfolio_webapi.models import db, User, Role

# scopes: package, session, module, class

@pytest.fixture(scope="session")
def app(user_data):
    app = create_app(config_name="testing")

    with app.app_context():
        # create all tables
        db.session.close()
        db.drop_all()
        db.create_all()

        # create user
        user_datastore = SQLAlchemyUserDatastore(db, User, Role)
        user_datastore.create_user(email=user_data['client_id'], password=user_data['hash'])
        db.session.commit()

    return app

@pytest.fixture(scope="session")
def user_data():
    return {
            'client_id': 'alen',
            'client_secret': 'pass',
            'hash': '$2y$12$ewH0KiznsgJDwiA1bpr1cuSf1NrY9wMCTGviHzmvPCgxmh762RkFa'
        }

@pytest.fixture(scope="session")
def token(app, user_data):
    """ Log in and return a valid token """
    login_res = app.test_client().post('/auth/login', data=user_data)
    access_token = json.loads(login_res.data.decode())['access_token']
    return access_token

@pytest.fixture(scope="session")
def auth_header(token):
    """ Generate the full authentication header with bearer token """
    return dict(Authorization="Bearer " + token)
