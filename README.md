# gnucash-portfolio-webapi

Web API for the GnuCash Portfolio suite

This project is a Web API interface to the GnuCash Portfolio library, including Asset Allocation and Price Database. The primary use is to serve data and perform actions for the web client apps.

The client is available at https://gnucash-portfolio.netlify.com/

## Scenario

WebAPI application can be instantiated

1.  on a local PC and exposed to the Internet using ngrok,
2.  on a mobile device directly

It needs to have access to

- GnuCash sqlite book
- price database
- asset allocation database

The client web apps need to be configured to use the app's url in order to utilize the API.

### PC

Execute `run.sh` script to start the server.
Create a tunnel with `ngrok http 5000`.

## Authentication

The app uses token security. Get a valid token with 

`POST localhost:5000/login` with `{"email":"email here", "password":"password here"}` as JSON request body.

Then use the token to authenticate to any API method with `Authentication-Token:"token here"` in the HTTP GET request.

## Development

`pip install -r requirements.txt`
`pip install -e .`

### Libraries

[Flask-HTTPAuth](https://flask-httpauth.readthedocs.io/en/latest/) provides token-based authentication for API methods. The library author has a series of relevant posts on his blog, like [Flask APIs](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xxiii-application-programming-interfaces-apis).
